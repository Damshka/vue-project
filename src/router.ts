import { createWebHistory, createRouter } from "vue-router";

import Home from "@/views/Home.vue";
import Statistic from "@/views/Statistic.vue";
import Page404 from "@/views/Page404.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/stat",
    name: "Statistic",
    component: Statistic,
  },
  {
    path: "/:catchAll(.*)",
    name: "Page404",
    component: Page404,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
