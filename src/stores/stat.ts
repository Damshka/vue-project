import { ref } from "vue";

export const stat = ref({
  totalClicks: 0,
  setTotalClicks(value: number) {
    this.totalClicks = value;
  },
  games: 0,
  setGames(value: number) {
    this.games = value;
  },
  reset: false,
  setReset(value: boolean) {
    this.reset = value;
  }
})
